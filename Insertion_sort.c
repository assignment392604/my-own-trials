# include <stdio.h>
void Insertion(int arr[],int length){
	int i;
    for (i=1; i<length;i++){
        int key=arr[i];
        int j=i-1;
        while(j>=0 && arr[j]>key){
            arr[j+1]=arr[j];         // In other words, it shifts the value at index i to index i+1, overwriting the value that was previously at index i+1.
            j=j-1;
        }
        arr[j+1]=key;
    }
};
int main(){
	int arr[]={3,6,8,0,1,5};
	int length=6;
    Insertion(arr, length);
    int i;
	
    printf("[");
    for (i=0;i<length;i++){
        printf("%d",arr[i]);
        if (i != length -1) 
        	printf(",");
    }
    printf("]");
    return 0;
}
