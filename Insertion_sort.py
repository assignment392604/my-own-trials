def Insertion(list):
   for i in range (1,len(list)):
        key=list[i]
        j=i-1
        while j>=0 and list[j]>key:
            list[j+1]=list[j]  #shifting the element one index to the right 
            j=j-1   #After shifting the element to the right, we decrement j to move to the previous position in the array and continue comparing.
        list[j+1]=key   # ensures that the key element is inserted into the array at the correct position without overwriting any existing elements 
a=[1,90,8,9,0]
Insertion(a)
print(a)