def Bubble_sort(list):
    n=len(list)
    for i in range(n):
        for j in range(0,n-1):    #When j reaches n-1, the condition list[j] > list[j+1] checks list[n-1] > list[n], which is out of range since list[n] does not exist.
            if(list[j]>list[j+1]):
                list[j], list[j+1] = list[j+1], list[j]  
    print(list)
a=[2,0,3,9,1]
Bubble_sort(a)              