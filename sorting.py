def Selection(list):
    for i in range(len(list)):
        min_ind=i
        for j in range (i+1,len(list)):
            if(list[min_ind]>list[j]):
                min_ind=j
            list[i],list[min_ind]=list[min_ind] ,list[i]     #Swapping is done outside the inner loop to ensure that we swap the minimum element found in the inner loop with the element at the correct position in the outer loop only once.
    print(list)

list=[2,5,8,9,0]
Selection(list)